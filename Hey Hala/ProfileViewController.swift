//
//  ProfileViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController

class ProfileViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var profileTab: RAMAnimatedTabBarItem!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTab.animation.iconSelectedColor = .white
        self.profileTab.animation.textSelectedColor = .white
        profileTab.playAnimation()
    }
    
    //MARK: IBAction
    @IBAction func profileAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetailViewController") as! ProfileDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func transactionAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func accountSetting(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountSettingViewController") as! AccountSettingViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func orderHistoryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as! MyOrdersViewController
        myVC.showBackButton = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func saveWhishlistAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SavedWishlistViewController") as! SavedWishlistViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

