//
//  TransactionViewController.swift
//  Hey Hala
//
//  Created by Apple on 17/04/21.
//

import UIKit
import iOSDropDown

class TransactionViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var sortBy: DropDown!
    
    let sortOption = ["Recent", "Newwst", "Oldest", "Date (From To)"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sortBy.optionArray = sortOption
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension TransactionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell") as! TransactionCell
        return cell
    }
    
    
}

class TransactionCell: UITableViewCell {
    //MARK: IBOutlets
    
}
