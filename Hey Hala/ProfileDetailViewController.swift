//
//  ProfileDetailViewController.swift
//  Hey Hala
//
//  Created by Apple on 17/04/21.
//

import UIKit

class ProfileDetailViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var userName: DesignableUITextField!
    @IBOutlet weak var mobileNumber: DesignableUITextField!
    @IBOutlet weak var userEmail: DesignableUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.attributedPlaceholder = NSAttributedString(string: "Ios User",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        mobileNumber.attributedPlaceholder = NSAttributedString(string: "+966-23432-34434",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        userEmail.attributedPlaceholder = NSAttributedString(string: "iosuser@mailinator.com",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
   
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
