//
//  AppDelegate.swift
//  Hey Hala
//
//  Created by Apple on 25/03/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window?.backgroundColor = .white
        // Override point for customization after application launch.
        return true
    }

}

