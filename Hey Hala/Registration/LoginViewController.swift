//
//  LoginViewController.swift
//  Hey Hala
//
//  Created by Apple on 02/04/21.
//

import UIKit

class LoginViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var appName: DesignableUILabel!
    @IBOutlet weak var loginLabel: DesignableUILabel!
    @IBOutlet weak var welcomeBackLabel: DesignableUILabel!
    @IBOutlet weak var countryCodeField: DesignableUITextField!
    @IBOutlet weak var contactNumberField: DesignableUITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryCodeField.attributedPlaceholder = NSAttributedString(string: "+966",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        contactNumberField.attributedPlaceholder = NSAttributedString(string: "Register mobile number",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    //MARK: IBActions
    @IBAction func loginAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationViewController") as! OTPVerificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}
