//
//  OTPVerificationViewController.swift
//  Hey Hala
//
//  Created by Apple on 02/04/21.
//

import UIKit

class OTPVerificationViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var heyhalaLabel: DesignableUILabel!
    @IBOutlet weak var otpLabel: DesignableUILabel!
    @IBOutlet weak var firstField: DesignableUITextField!
    @IBOutlet weak var secondField: DesignableUITextField!
    @IBOutlet weak var thirdField: DesignableUITextField!
    @IBOutlet weak var fourthField: DesignableUITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firstField.delegate = self
        self.secondField.delegate = self
        self.thirdField.delegate = self
        self.fourthField.delegate = self
        self.firstField.becomeFirstResponder()

    }
    
    //MARK: IBActions

    @IBAction func confirmAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func resendAction(_ sender: Any) {
    }
    
}

extension OTPVerificationViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if(textField == firstField){
            if(textField.text!.count == 0){
                textField.text = string
                self.secondField.becomeFirstResponder()
                return true
            }else if (isBackSpace == -92) {
                textField.text = ""
            }
        }else if(textField == secondField){
            if(textField.text!.count == 0){
                textField.text = string
                self.thirdField.becomeFirstResponder()
                return true
                
            }else if (isBackSpace == -92) {
                textField.text = ""
                self.firstField.becomeFirstResponder()
            }
        }else if(textField == thirdField){
            if(textField.text!.count == 0){
                textField.text = string
                self.fourthField.becomeFirstResponder()
                return true
            }else if (isBackSpace == -92) {
                textField.text = ""
                self.secondField.becomeFirstResponder()
            }
        }else if(textField == fourthField){
            if(textField.text!.count == 0){
                textField.text = string
                self.fourthField.resignFirstResponder()
                return true
            }else if (isBackSpace == -92) {
                textField.text = ""
                self.thirdField.becomeFirstResponder()
            }
        }
        return false
    }
}
