//
//  NavigationController.swift
//  Hey Hala
//
//  Created by qw on 01/01/21.
//

import UIKit
import CoreLocation


class NavigationController: UIViewController, CLLocationManagerDelegate {
    //MARK: IBOutlets
    
    
    static let shared = NavigationController()
    lazy var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
   
    
//
//    func getPreferredRestaurant(){
//
//        let location = Singleton.shared.selectedLocation
//        let coordinate = self.getCurrentLocation()
//        if(location.id == nil){
//            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PREFERRED_RESTAURANT, method: .post, parameter: ["latitude":Singleton.shared.currentLocation.latitude,"longitude":Singleton.shared.currentLocation.longitude], objectClass: GetLocationDetail.self, requestCode: U_GET_PREFERRED_RESTAURANT, userToken: nil) { (response) in
//                let selectedLoc = try! JSONEncoder().encode(response.response)
//                Singleton.shared.selectedLocation = response.response
//                UserDefaults.standard.set(selectedLoc, forKey: UD_SELECTED_RESTAURANT)
//                self.getMenuType()
//
//            }
//        }else {
//            self.getMenuType()
//        }
//    }
    
//    func getCurrentLocation(){
//        if(self.hasLocPermission()){
//            if let loc = locationManager.location {
//                Singleton.shared.currentLocation = loc.coordinate
//            }
//        }
//    }
//
//    func hasLocPermission() -> Bool {
//        var hasPermission = false
//        if CLLocationManager.locationServicesEnabled() {
//            switch CLLocationManager.authorizationStatus() {
//            case .notDetermined, .restricted, .denied:
//                hasPermission = false
//            case .authorizedAlways, .authorizedWhenInUse:
//                hasPermission = true
//            }
//        } else {
//            hasPermission = false
//        }
//        return hasPermission
//    }
    
}




