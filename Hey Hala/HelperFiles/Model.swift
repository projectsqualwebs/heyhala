//
//  Model.swift
//  Hey Hala
//
//  Created by qw on 09/01/21.
//

import Foundation

struct ErrorResponse: Codable{
    var message: String?
    var status: Int?
}

struct SuccessResponse: Codable{
    var message: String?
    var status: Int?
}
