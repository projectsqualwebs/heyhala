//
//  Gradient.swift
//  JAN-MUSIC
//
//  Created by Manisha  Sharma on 20/11/2018.
//  Copyright © 2018 Qualwebs. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable
    public var startColor: UIColor = .clear {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    public var endColor: UIColor = blackColor.withAlphaComponent(0.6) {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    public var cornerRadius:CGFloat = 0.0
    
    @IBInspectable
    public var circleView:Bool = false
    
    private lazy var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x:0, y:1)
        return gradientLayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = self.bounds
        if circleView {
            self.layer.cornerRadius = self.frame.size.width/2
            gradientLayer.cornerRadius = self.frame.size.width/2
        } else {
            gradientLayer.cornerRadius = cornerRadius
        }
    }
}




