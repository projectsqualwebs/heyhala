//
//  Constants.swift
//  Hey Hala
//
//  Created by qw on 23/12/20.

import UIKit

let primaryColor = UIColor(red: 111/255, green: 1/255, blue: 1/255, alpha: 1)//#6F0101
let darkBrown = UIColor(red: 90/255, green: 2/255, blue: 2/255, alpha: 1) //#5A0202
let blackColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1) //#000000
let darkGrayColor = UIColor(red: 44/255, green: 44/255, blue: 44/255, alpha: 1) //#2C2C2C
let grayColor = UIColor(red: 58/255, green: 56/255, blue: 56/255, alpha: 1) //#3A3838

let U_BASE = "https://admin.farmersfreshkitchen.com/api/"
let U_IMAGE_BASE =  "http://54.219.128.62/api/public/storage/"


//User Defaults
let UD_USER_DETAIl = "userDetail"
let UD_TOKEN = "user_token"

//Notifications


