//
//  AccountSettingViewController.swift
//  Hey Hala
//
//  Created by Apple on 19/04/21.
//

import UIKit

class AccountSettingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
