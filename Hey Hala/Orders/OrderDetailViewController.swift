//
//  OrderDetailViewController.swift
//  Hey Hala
//
//  Created by Apple on 14/04/21.
//

import UIKit

class OrderDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBActions
    @IBAction func trackOrderAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
