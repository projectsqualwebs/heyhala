//
//  OrderConfirmationViewController.swift
//  Hey Hala
//
//  Created by Apple on 20/04/21.
//

import UIKit

class OrderConfirmationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBActions
    
    @IBAction func trackOrderAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackOrderViewController") as! TrackOrderViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
