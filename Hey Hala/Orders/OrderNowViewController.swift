//
//  OrderNowViewController.swift
//  Hey Hala
//
//  Created by Apple on 14/04/21.
//

import UIKit

class OrderNowViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    
    @IBAction func orderNowAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        myVC.showBackButton = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}
