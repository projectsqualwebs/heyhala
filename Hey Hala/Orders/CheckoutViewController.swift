//
//  CheckoutViewController.swift
//  Hey Hala
//
//  Created by Apple on 20/04/21.
//

import UIKit

class CheckoutViewController: UIViewController, SlideButtonDelegate {
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        if(status == "Unlocked"){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderConfirmationViewController") as! OrderConfirmationViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    //MARK: IBOutlets
    @IBOutlet weak var slidingButton: MMSlidingButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.slidingButton.delegate = self
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
