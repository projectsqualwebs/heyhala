//
//  AddAddressViewController.swift
//  Hey Hala
//
//  Created by Apple on 17/04/21.
//

import UIKit

class AddAddressViewController: UIViewController {
    //MARK: IBOutlets
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addAddressAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
}

extension AddAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell") as! AddressTableCell
        return cell
    }
    
    
}

class AddressTableCell: UITableViewCell {
    //MARK: IBOutlets
    
}
