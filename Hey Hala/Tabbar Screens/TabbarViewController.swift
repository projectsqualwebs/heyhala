//
//  TabbarViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController

class TabbarViewController: RAMAnimatedTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = .white
        self.tabBar.standardAppearance.selectionIndicatorTintColor = .white
        self.tabBar.unselectedItemTintColor = .white
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .selected)
        // Do any additional setup after loading the view.
    }
}
