//
//  HomeViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController
import ViewAnimator


class HomeViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var homeTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var collectionView: UICollectionView!
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeTab.animation.textSelectedColor = .white
        homeTab.animation.iconSelectedColor = .white
        homeTab.playAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.animate()
    }
    
    @IBAction func animate() {
           // Combined animations example
           let fromAnimation = AnimationType.vector(CGVector(dx: 30, dy: 0))
           let zoomAnimation = AnimationType.zoom(scale: 0.2)
           let rotateAnimation = AnimationType.rotate(angle: CGFloat.pi/6)
            
           UIView.animate(views: collectionView.visibleCells,
                          animations: [fromAnimation], delay: 0)
        
       }


    //MARK: IBActions
    
    @IBAction func signupAction(_ sender: Any) {
    }
    
    @IBAction func languageAction(_ sender: Any) {
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollection", for: indexPath) as!  CategoryCollection
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/2)-15, height: (collectionView.frame.width/2)-15)
    }
    
    
}


class CategoryCollection: UICollectionViewCell{
    //MARK: IBOutlets
    @IBOutlet weak var categoryImage: ImageView!
    @IBOutlet weak var categoryName: DesignableUILabel!
    @IBOutlet weak var categoryDesc: DesignableUILabel!
}
