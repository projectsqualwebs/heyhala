//
//  MyCartViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController

class MyCartViewController: UIViewController, SlideButtonDelegate {
    func buttonStatus(status: String, sender: MMSlidingButton) {
        if(status == "Unlocked"){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var addressTable: ContentSizedTableView!
    @IBOutlet weak var itemTable: ContentSizedTableView!
    @IBOutlet weak var slideButtonView: MMSlidingButton!
    @IBOutlet weak var cartTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var backView: UIView!
    
    var showBackButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.slideButtonView.delegate = self
        if(self.showBackButton){
            self.backView.isHidden = false
        }
        self.addressTable.reloadData()
        self.itemTable.reloadData()
        self.cartTab.animation.iconSelectedColor = .white
        self.cartTab.animation.textSelectedColor = .white
        cartTab.playAnimation()
        // Do any additional setup after loading the view.
    }
}

extension MyCartViewController: UITableViewDelegate ,  UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == itemTable){
            return 4
        }else {
        return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == itemTable){
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell") as! ItemTablecell
             cell.segmentControl.layer.cornerRadius = 0
             cell.segmentControl.tintColor = .white
            cell.segmentControl.tintColor = .white
            cell.segmentControl.selectedSegmentTintColor = .white
            cell.segmentControl.backgroundColor = .clear
            
             cell.segmentControl.layer.masksToBounds = true
          //  cell.segmentControl.clipsToBounds = true
            // selected option color
            cell.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)

            // color of other options
            cell.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTable") as! AddressTable
            return cell
        }
    }
    
    
}

class AddressTable: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var streetAddress: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    
    
    var changeBtn:(()->Void)? = nil
    
    //MARK: IBActions
    @IBAction func changeAction(_ sender: Any) {
        if let changeBtn = self.changeBtn {
            changeBtn()
        }
    }
    
}

class ItemTablecell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var itemName: DesignableUILabel!
    @IBOutlet weak var itemPrice: DesignableUILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
   
}
