//
//  MyOrdersViewController.swift
//  Hey Hala
//
//  Created by Apple on 31/03/21.
//

import UIKit
import RAMAnimatedTabBarController

class MyOrdersViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var myOrderTab: RAMAnimatedTabBarItem!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchView: View!
    
    var showBackButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(showBackButton){
            self.searchView.isHidden = true
            self.backView.isHidden = false
        }
        self.myOrderTab.animation.iconSelectedColor = .white
        self.myOrderTab.animation.textSelectedColor = .white
        myOrderTab.playAnimation()
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTablecell") as! ItemTablecell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
